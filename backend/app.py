import os
from bottle import route, static_file, template, run


@route(r'/articles/<article:re:\w*\.json>')
def static_article(article):
    json = static_file(article, root='articles/', mimetype='application/json')
    json.set_header('Access-Control-Allow-Origin', '*')
    return json

@route(r'<filepath:re:/.*\.js>')
def static_js(filepath):
    file = filepath.split('/')[-1]
    return static_file(file, root='static/js/', mimetype='text/javascript')


@route(r'/static/css/<file:re:[\w\d.]*\.css>')
def static_css(file):
    return static_file(file, root='static/css/', mimetype='text/css')


@route('/')
def index():
    return template('index.html')


run(host="0.0.0.0", port=int(os.environ.get("PORT", 5000)))
